/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Atores;

import Pedido.Pedido;

/**
 *
 * @author bmatias
 */
public class Cliente extends Pessoa {
    private Pedido pedido;
    
    public Cliente(Pedido pedido, String nome) {
        super(nome);
        this.pedido = pedido;
    }

    public Cliente() {
    }
    
    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }
    
    
}
