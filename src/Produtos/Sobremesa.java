/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produtos;

/**
 *
 * @author bmatias
 */
public class Sobremesa extends Produto {

    public Sobremesa(String nome, float preco, Stock stock) {
        super(nome, preco, stock);
    }

    public Sobremesa() {
        setTipo(3);
    }
      
}
