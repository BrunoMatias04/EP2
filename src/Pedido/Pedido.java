/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pedido;

import Pagamento.Cartao;
import Pagamento.Dinheiro;
import Pagamento.Pagamento;
import Produtos.Produto;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author bmatias
 */
public class Pedido {
    private String observacao;
    private float TotalDaCompra;
    private String data;
    private ArrayList<Produto> ListaDePedidos;
    private Pagamento pagamento;
    
    public Pedido(String observacao, float TotalDaCompra, String data, ArrayList<Produto> ListaDePedidos) {
        this.observacao = observacao;
        this.TotalDaCompra = TotalDaCompra;
        this.data = data;
        this.ListaDePedidos = ListaDePedidos;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }

    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }
    
    public void Pagar(int i){
        if(i == 0){
            pagamento = new Dinheiro();
            pagamento.Pagar();
        }
        else if(i == 1){
            pagamento = new Cartao();
            pagamento.Pagar();
        }
    }
        public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Pedido() {
        this.ListaDePedidos = new ArrayList<Produto>();
        this.pagamento = new Pagamento();
    }
    
    public void AddNaLista(Produto produtoAux){
        ListaDePedidos.add(produtoAux);
}
    public void RemoverLista(Produto produtoAux){
        ListaDePedidos.remove(produtoAux);
    }
    public void CalculaTotalAPagar(){
        int aux , i=0;
        float SomaDeTudo = 0;
        aux = ListaDePedidos.size();
        while(i < aux){
            SomaDeTudo += ListaDePedidos.get(i).getPreco();
            i++;
        }
        setTotalDaCompra(SomaDeTudo);
    }
    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public float getTotalDaCompra() {
        return TotalDaCompra;
    }

    public void setTotalDaCompra(float TotalDaCompra) {
        this.TotalDaCompra = TotalDaCompra;
    }

    public ArrayList<Produto> getListaDePedidos() {
        return ListaDePedidos;
    }

    public void setListaDePedidos(ArrayList<Produto> ListaDePedidos) {
        this.ListaDePedidos = ListaDePedidos;
    }
    
}
