/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pagamento;

/**
 *
 * @author bmatias
 */
public class Pagamento {
    private boolean pagamento;
    public Pagamento() {
        this.pagamento = false;
    }

    public boolean isPagamento() {
        return pagamento;
    }

    public void setPagamento(boolean pagamento) {
        this.pagamento = pagamento;
    }
    
    public void Pagar(){
        pagamento = true;
    }
}
