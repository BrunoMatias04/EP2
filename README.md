# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

-UML disponivel na pasta do projeto.

# Para Executar o programa

- Primeiramente deve se utilizar uma IDE como : Netbeans ou Eclipse.
- Depois abrir a pasta do arquivo na IDE e Executar o projeto.

# Abrindo o programa ...    

- irá se deparar com uma tela de login , onde o usuario poderá fazer seu cadastro para prosseguir.
- Logo após fazer o login abrirá a tela principal onde a várias possibilidades como: Cadastrar Novo produto , Alterar 
estoque de um produto , Fazer e comfirmar um pedido , Atualizar o programa e por fim efetuar o pagamento.   
- Se for fazer um pedido , ao abrir o programa primeiramente faça o cadastro dos produtos que o restaurante tem , e logo depois aperte para atualizar.
- irá atulizar a lista de produtos onde vc poderá ver o nome , preço e quantidade do produto cadastrado.
- logo no começo tem para bota o nome do cliente e a data do pedido.
- depois disso vc poderar digitar o nome do produto na caixa embaixo da lista e adicionar ao pedido(onde tambem poderá remover um produto se desejar).
- logo depois tem a caixa de texto onde poderá colocar um "obs" que o cliente fará  ou não.
- do lado aparecerá o preço total do pedido.
- e para finalizar o botão de confirmar o pedido.
- Após ter feito um pedido , ele aparecerá ae lado direito na lista de pedidos feitos , que irá conter o numero o nome a data e se foi pago ou não.
- se o pedido não foi pago , aparecerá o preço tambem.
- encima da lista de pedidos há as opções de alterar o estoque e cadastrar um novo produto.
- embaixo da lista de pedidos tera a opção de pagamento dos pedidos , onde o usuario irá colocar o numero do pedido , escolher entre cartão ou 
dinheiro e efetuar o pagamento.
- Se o usuario quiser fazer outro pedido é so voltar ao lado esquerdo do programa e preecher de novo.

# Para testar o programa

- Abra a pasta do arquivo na IDE
- Va na pasta de teste ,abra o pacote e execute a classe teste.