/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Teste;

import Atores.Cliente;
import Atores.Empregado;
import Pagamento.Cartao;
import Pagamento.Dinheiro;
import Pedido.Pedido;
import Produtos.Bebida;
import Produtos.Prato;
import Produtos.Sobremesa;
import Produtos.Stock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tocomfome.BancoDeDados;

/**
 *
 * @author bmatias
 */
public class Teste {
    
    public Teste() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void TesteComponentes() {
    Cliente cliente = new Cliente();
    Assert.assertNull(cliente.getPedido());
    Empregado empregado = new Empregado();
    Cartao cartao = new Cartao();
    Dinheiro dinheiro = new Dinheiro();
    Pedido pedido = new Pedido();
    Bebida bebida = new Bebida();
    Prato prato = new Prato();
    Sobremesa sobremesa = new Sobremesa();
    Stock estoque = new Stock();
    BancoDeDados banco = new BancoDeDados();
    empregado.setLogin("Joao");
    Assert.assertEquals("Joao", empregado.getLogin());
    empregado.setSenha("123");
    Assert.assertEquals("123", empregado.getSenha());
    estoque.setQuantidade(10);
    Assert.assertEquals(10, estoque.getQuantidade());
    prato.setNome("Frango");
    Assert.assertEquals("Frango", prato.getNome());
    prato.setPreco(3);
    Assert.assertNotNull(prato.getPreco());
    prato.setStock(estoque);
    Assert.assertEquals(10, prato.getStock().getQuantidade());
    bebida.setNome("Coca");
    bebida.setPreco(3);
    bebida.setStock(estoque);
    sobremesa.setNome("Bolo");
    sobremesa.setPreco(3);
    sobremesa.setStock(estoque);
    pedido.AddNaLista(prato);
    pedido.AddNaLista(bebida);
    pedido.AddNaLista(sobremesa);
    pedido.setData("13/11/16");
    pedido.setObservacao("oi");
    pedido.setPagamento(dinheiro);
    cliente.setNome("Bruno");
    cliente.setPedido(pedido);
    banco.addProduto(prato);
    banco.addProduto(bebida);
    banco.addProduto(sobremesa);
    banco.addEmpregado(empregado);
    banco.addPedido(cliente);
    
    Assert.assertArrayEquals(banco.getBancoDeProdutos().toArray(),cliente.getPedido().getListaDePedidos().toArray());
    Assert.assertFalse(cliente.getPedido().getPagamento().isPagamento());
    
    cliente.getPedido().Pagar(0);
    
    Assert.assertTrue(cliente.getPedido().getPagamento().isPagamento());
    
    cliente.getPedido().RemoverLista(prato);
    banco.removerProduto(prato);
    cliente.getPedido().CalculaTotalAPagar();
    
    Assert.assertEquals(6.0, cliente.getPedido().getTotalDaCompra(),0.00001);
    Assert.assertArrayEquals(banco.getBancoDeProdutos().toArray(),cliente.getPedido().getListaDePedidos().toArray());
    
    }
}
